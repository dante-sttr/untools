
<!-- README.md is generated from README.Rmd. Please edit that file -->

# untools <img src='man/figures/logo.png' align="right" width="120" />

The `untools` package provides a suite of tools to facilitate acquiring,
processing, and visualizing United Nations data. We currently provide
support, in at least some capacity, for the following United Nations
data sets:

  - The United Nations High Commissioner for Refugees: Populations
    Figures
      - *UNHCR data on displacement at the end of the year.*
  - The United Nations High Commissioner for Refugees: Populations
    Figures Demographics
      - *UNHCR data on displacement at the end of the year by age and
        gender.*
  - The United Nations High Commissioner for Refugees: Asylum
    Applications
      - *Asylum claims submitted during the year.*
  - The United Nations High Commissioner for Refugees: Asylum Decisions
      - *Decisions taken on asylum claims during the year.*
  - Internally Displaced Persons
      - *Internal displacement due to conflict and violence at the end
        of the year as reported by the International Displacement
        Monitoring Center (IDMC).*
  - United Nations Relief Works Agency: Palestinian Refugees
      - *Registered Palestine refugees at the end of the year.*
  - United Nations Department of Economic and Social Affairs Population
    Division: International Migrant Stocks Time Series (2017 and 2019
    Revisions)
      - *Official statistics on the foreign-born or the foreign
        population by host country.*

This welcome page presents a brief introduction to the `untools`
package. For more detailed information and reference materials for
`untools` please refer to:

  - [*untools* Reference
    Manual](https://dante-sttr.gitlab.io/untools/index.html)
  - [Exploring United Nations Refugee & Asylum Data With the *untools*
    Package](https://dante-sttr.gitlab.io/untools/articles/explore-unhcr.html)
  - [Exploring United Nations Migrant Stock Data With the *untools*
    Package](https://dante-sttr.gitlab.io/untools/articles/explore-unstocks.html)
  - [Creating Georectified Maps of Refugee and Asylum
    Flows](https://dante-sttr.gitlab.io/untools/articles/geo-unref.html)

# Installation

You can install the released version of untools from
[GitLab](https://https://gitlab.com/dante-sttr/untools) with:

``` r
devtools::install_gitlab("dante-sttr/untools", dependencies=TRUE)
```

### Installation With Windows

To install R packages over Git on a Windows system, you must install
Rtools first. The latest version of Rtools is available
[here](https://cran.r-project.org/bin/windows/Rtools/). Furthermore, you
may experience difficulty installing R packages over Git if you utilize
a Windows machine on a network with Active Directory or shared network
drives. To enable proper package installation under these circumstances
please follow [this
guide](https://dante-sttr.gitlab.io/dante-vignettes/windows-pkg-inst/windows-pkg-inst.html).

# Usage

Read in the United Nations Migrant Stock (2019 Revision) data with the
`getUNstocks` function. This function will automatically parse the
spreadsheet and provide simple formatting and processing.

``` r
library(untools)

stocks19<-untools::getUNstocks(version = '2019')
#> Warning in `[.data.table`(stocks, , `:=`(id, NULL)): Column 'id' does not exist
#> to remove
```

<table>

<thead>

<tr>

<th style="text-align:right;">

year

</th>

<th style="text-align:left;">

host

</th>

<th style="text-align:left;">

host\_iso3

</th>

<th style="text-align:left;">

origin

</th>

<th style="text-align:left;">

origin\_iso3

</th>

<th style="text-align:right;">

stock

</th>

<th style="text-align:right;">

code

</th>

</tr>

</thead>

<tbody>

<tr>

<td style="text-align:right;">

2019

</td>

<td style="text-align:left;">

Argentina

</td>

<td style="text-align:left;">

ARG

</td>

<td style="text-align:left;">

Afghanistan

</td>

<td style="text-align:left;">

AFG

</td>

<td style="text-align:right;">

9

</td>

<td style="text-align:right;">

32

</td>

</tr>

<tr>

<td style="text-align:right;">

2015

</td>

<td style="text-align:left;">

Argentina

</td>

<td style="text-align:left;">

ARG

</td>

<td style="text-align:left;">

Afghanistan

</td>

<td style="text-align:left;">

AFG

</td>

<td style="text-align:right;">

9

</td>

<td style="text-align:right;">

32

</td>

</tr>

<tr>

<td style="text-align:right;">

2010

</td>

<td style="text-align:left;">

Argentina

</td>

<td style="text-align:left;">

ARG

</td>

<td style="text-align:left;">

Afghanistan

</td>

<td style="text-align:left;">

AFG

</td>

<td style="text-align:right;">

9

</td>

<td style="text-align:right;">

32

</td>

</tr>

<tr>

<td style="text-align:right;">

2005

</td>

<td style="text-align:left;">

Argentina

</td>

<td style="text-align:left;">

ARG

</td>

<td style="text-align:left;">

Afghanistan

</td>

<td style="text-align:left;">

AFG

</td>

<td style="text-align:right;">

15

</td>

<td style="text-align:right;">

32

</td>

</tr>

<tr>

<td style="text-align:right;">

2000

</td>

<td style="text-align:left;">

Argentina

</td>

<td style="text-align:left;">

ARG

</td>

<td style="text-align:left;">

Afghanistan

</td>

<td style="text-align:left;">

AFG

</td>

<td style="text-align:right;">

20

</td>

<td style="text-align:right;">

32

</td>

</tr>

<tr>

<td style="text-align:right;">

1995

</td>

<td style="text-align:left;">

Argentina

</td>

<td style="text-align:left;">

ARG

</td>

<td style="text-align:left;">

Afghanistan

</td>

<td style="text-align:left;">

AFG

</td>

<td style="text-align:right;">

20

</td>

<td style="text-align:right;">

32

</td>

</tr>

<tr>

<td style="text-align:right;">

1990

</td>

<td style="text-align:left;">

Argentina

</td>

<td style="text-align:left;">

ARG

</td>

<td style="text-align:left;">

Afghanistan

</td>

<td style="text-align:left;">

AFG

</td>

<td style="text-align:right;">

20

</td>

<td style="text-align:right;">

32

</td>

</tr>

<tr>

<td style="text-align:right;">

2019

</td>

<td style="text-align:left;">

Australia

</td>

<td style="text-align:left;">

AUS

</td>

<td style="text-align:left;">

Afghanistan

</td>

<td style="text-align:left;">

AFG

</td>

<td style="text-align:right;">

59798

</td>

<td style="text-align:right;">

36

</td>

</tr>

<tr>

<td style="text-align:right;">

2015

</td>

<td style="text-align:left;">

Australia

</td>

<td style="text-align:left;">

AUS

</td>

<td style="text-align:left;">

Afghanistan

</td>

<td style="text-align:left;">

AFG

</td>

<td style="text-align:right;">

49550

</td>

<td style="text-align:right;">

36

</td>

</tr>

<tr>

<td style="text-align:right;">

2010

</td>

<td style="text-align:left;">

Australia

</td>

<td style="text-align:left;">

AUS

</td>

<td style="text-align:left;">

Afghanistan

</td>

<td style="text-align:left;">

AFG

</td>

<td style="text-align:right;">

30840

</td>

<td style="text-align:right;">

36

</td>

</tr>

</tbody>

</table>

Plot a time series of the top migrant stock populations in the United
States using the ISO3 character code.

``` r
usa.stocks.ts<-plot(stocks19, country = "USA")
```

<img src="man/figures/README-unnamed-chunk-3-1.png" width="100%" />

Or view a barplot of only the year 2000.

``` r
usa.stocks.static<-plot(stocks19, country = "USA", mode = 'static', yr=2005)
```

<img src="man/figures/README-unnamed-chunk-4-1.png" width="100%" />
